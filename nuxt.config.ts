// https://nuxt.com/docs/api/configuration/nuxt-config
import { resolve } from "path"
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      title: "The Movie Finder"
    }
  },
  alias : {
    "@": resolve(__dirname, "/"),
  },
  css: ["~/assets/main.css"],
  modules: [
    '@nuxtjs/tailwindcss'
  ],
  runtimeConfig: {
    TMDB_API_KEY: process.env.TMDB_API_KEY,
  },
})
