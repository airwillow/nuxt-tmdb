export default defineEventHandler(async (event) => {
    const { TMDB_API_KEY } = useRuntimeConfig();
    const { search } = getQuery(event);
    try {
        return await $fetch(`https://api.themoviedb.org/3/search/movie?&include_adult=false&language=fr-FR&page=1&api_key=${TMDB_API_KEY}&query=${search}`);
    } catch (error) {
        return null;
    }
});